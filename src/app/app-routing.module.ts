import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EtudiantComponent } from './pages/etudiant/etudiant.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { MatiereComponent } from './pages/matiere/matiere.component';
import { NoteComponent } from './pages/note/note.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { AuthGuardEtudiantService } from './services/auth-guard-etudiant.service';
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'examen',
    component: ExamenComponent,
    // canActivate: [AuthGuardEtudiantService],
  },
  {
    path: 'matiere',
    component: MatiereComponent,
  },
  {
    path: 'note/:id',
    component: NoteComponent,
    canActivate: [AuthGuardEtudiantService],
  },
  {
    path: 'etudiant',
    component: EtudiantComponent,
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./pages/dashboard/dashboard.module').then(
        (m) => m.DashboardModule
      ),
  },
  {
    path: '**',
    component: PageNotFoundComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
