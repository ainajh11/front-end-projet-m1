import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddMatiereComponent } from '../add-matiere/add-matiere.component';
import { MatiereService } from './../../services/matiere.service';

@Component({
  selector: 'app-list-matiere',
  templateUrl: './list-matiere.component.html',
  styleUrls: ['./list-matiere.component.scss'],
})
export class ListMatiereComponent implements OnInit {
  public matieres: any[] = [];
  niveauValue: string = '';
  parcoursValue: string = '';
  constructor(
    private matiereService: MatiereService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getAllMatiere();
  }
  getAllMatiere() {
    this.matiereService.getAllMatiere().then((res: any) => {
      // console.log(res);
      this.matieres = res.matieres;
    });
  }

  openAddDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '65%';
    dialogConfig.data = { type: 'add' };
    const openDial = this.dialog.open(AddMatiereComponent, dialogConfig);
    openDial;
    openDial.afterClosed().subscribe(() => {
      this.getAllMatiere();
    });
  }
  openEditDialog(id: String) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '65%';
    dialogConfig.data = { type: 'update', id: id };
    const openDial = this.dialog.open(AddMatiereComponent, dialogConfig);
    openDial;
    openDial.afterClosed().subscribe(() => {
      this.getAllMatiere();
    });
  }

  deleteEtudiant(id: String) {
    this.matiereService.deleteMatiere(id).then((res: any) => {
      this.getAllMatiere();
    });
  }

  searchByNiveau() {
    // console.log(this.niveauValue + this.parcoursValue);
    if (this.niveauValue != '' && this.parcoursValue != '') {
      this.matiereService
        .searchMatiereByNivParc(this.niveauValue, this.parcoursValue)
        .then((res: any) => {
          // console.log(res.length);

          this.matieres = res;
        });
    }
  }

  refresh() {
    this.getAllMatiere();
    this.niveauValue = '';
    this.parcoursValue = '';
  }
}
