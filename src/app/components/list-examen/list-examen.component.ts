import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-list-examen',
  templateUrl: './list-examen.component.html',
  styleUrls: ['./list-examen.component.scss'],
})
export class ListExamenComponent implements OnInit {
  questions: any = [];
  search: any = {
    identifiant: '',
  };
  constructor(
    public router: Router,
    private questionService: QuestionService
  ) {}

  ngOnInit(): void {
    this.getAllQuestions();
  }

  goToNewExam() {
    this.router.navigate(['/dashboard/new-examen']);
  }

  getAllQuestions() {
    this.questionService.getAllQuestions().then((res: any) => {
      // console.log(res[0]);
      this.questions = res[0];
    });
  }

  deleteQuestion(id: string) {
    this.questionService.deleteQuestion(id).then((res: any) => {
      console.log(res);
      if (res.message === 'Suppression avec succès') {
        this.getAllQuestions();
      }
    });
  }

  getQuestionByIdentifiant() {
    // console.log(this.search.identifiant);
    if (this.search.identifiant !== '') {
      this.questionService
        .getQuestionIdentifiant(this.search.identifiant)
        .then((res: any) => {
          this.questions = res[0];
          // console.log(res[0]);
        });
    } else {
      this.getAllQuestions();
    }
  }

  onRefresh() {
    this.getAllQuestions();
    this.search = { identifiant: '' };
  }

  editQuestion(id: string) {
    console.log(id);
    this.router.navigate([`/dashboard/update-examen/${id}`]);
  }
}
