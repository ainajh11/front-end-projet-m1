import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IndexeddbService } from 'src/app/services/idb.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  isCollapsed = false;
  isEnable = false;
  constructor(public router: Router, private idb: IndexeddbService) {}

  ngOnInit(): void {}

  goToDashboard() {
    this.router.navigate(['/dashboard']);
  }

  logOut() {
    // this.idb.delete('token_exam');
    // this.idb.delete('user_info');
    this.removeToken('token_exam');
    this.removeToken('user_info');
    this.router.navigate(['/']);
  }

  removeToken(token: any) {
    this.idb.delete(token);
  }
}
