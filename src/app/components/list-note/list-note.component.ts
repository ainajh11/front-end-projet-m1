import { Component, OnInit } from '@angular/core';
import { ExamenService } from 'src/app/services/examen.service';

@Component({
  selector: 'app-list-note',
  templateUrl: './list-note.component.html',
  styleUrls: ['./list-note.component.scss'],
})
export class ListNoteComponent implements OnInit {
  public examens: any[] = [];
  niveauValue: string = '';
  parcoursValue: string = '';
  constructor(private examenService: ExamenService) {}

  ngOnInit(): void {
    this.getAllEtudiants();
  }
  getAllEtudiants() {
    this.examenService.getAllNote().then((res: any) => {
      this.examens = res.results[0];
    });
  }

  // deleteEtudiant(id: String) {
  //   this.etudiantService.deleteEtudiant(id).then((res: any) => {
  //     this.getAllEtudiants();
  //   });
  // }

  // searchByNiveau() {
  //   // console.log(this.niveauValue + this.parcoursValue);
  //   if (this.niveauValue != '' && this.parcoursValue != '') {
  //     this.etudiantService
  //       .searchEtudiantByNivParc(this.niveauValue, this.parcoursValue)
  //       .then((res: any) => {
  //         // console.log(res.length);

  //         this.etudiants = res;
  //       });
  //   }
  // }

  refresh() {
    this.getAllEtudiants();
    this.niveauValue = '';
    this.parcoursValue = '';
  }
}
