import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatiereService } from 'src/app/services/matiere.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-add-examen',
  templateUrl: './add-examen.component.html',
  styleUrls: ['./add-examen.component.scss'],
})
export class AddExamenComponent implements OnInit {
  panelOpenState = false;
  openQuestion = false;
  divs: number[] = [];
  reponse: string = '';
  reponseTrue: string[] = [];
  messageSuccess = '';
  matiere: any;
  questions: any = {
    question: '',
    time: 0,
    all_reponse: [],
    true_reponse: [],
    identifiant: '',
    matiere: '',
    semestre: '',
  };

  titreQuestion = '';
  constructor(
    private questionService: QuestionService,
    private matiereService: MatiereService,
    public router: Router,
    private paramRouter: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getAllMatieres();
    this.isOpenUpdate();
  }

  openQ() {
    this.openQuestion = true;
  }

  editQuestion() {
    this.openQuestion = false;
  }

  addResponse() {
    // console.log(this.reponse);

    this.questions.all_reponse.push(this.reponse);
    this.reponse = '';
  }

  removeResponse(i: any) {
    this.questions.all_reponse.splice(i, 1);
    // console.log(i);
    // console.log(this.questions.all_reponse);
  }

  onListSelectionChange(event: any) {
    this.questions.true_reponse = event.source._value;
  }

  saveQuestion() {
    if (
      this.questions.question === '' ||
      this.questions.semestre === '' ||
      this.questions.question === 0 ||
      this.questions.all_reponse.length === 0 ||
      this.questions.true_reponse.length === 0 ||
      this.questions.identifiant === '' ||
      this.questions.matiere === ''
    ) {
      this.messageSuccess =
        "Verifier s'il ya un champs manqué ou des réponses non séléctionnés";
    } else {
      console.log('success', this.questions);
      this.questionService.addQuestion(this.questions).then((res: any) => {
        // console.log(res);
        if (res.success) {
          this.messageSuccess = res.matiere;
          setTimeout(() => {
            this.messageSuccess = '';
            this.openQuestion = false;
            this.questions.all_reponse = [];
            this.questions.true_reponse = [];
            this.questions.question = '';
          }, 2000);
        } else {
          this.messageSuccess =
            'Question déjà inséré ou tu peut verifier les champs stp';
        }
      });
    }
  }

  getAllMatieres() {
    this.matiereService.getAllMatiere().then((res: any) => {
      // console.log(res.matieres);
      this.matiere = res.matieres;
    });
  }

  goHome() {
    this.router.navigate(['/dashboard/examen']);
  }

  isOpenUpdate() {
    if (this.router.url.includes('/update-examen')) {
      this.titreQuestion = 'Modifier';
      this.questionService
        .getQuestionID(this.paramRouter.snapshot.params.id)
        .then((res: any) => {
          // console.log(res[0][0]);
          const ress = res[0][0];
          this.openQuestion = true;
          this.panelOpenState = true;
          this.questions = {
            question: ress.question,
            all_reponse: ress.all_reponse,
            true_reponse: ress.true_reponse,
            identifiant: ress.identifiant,
            matiere: ress.matiere[0]._id,
            semestre: ress.semestre,
            time: ress.time,
          };
        });
    } else {
      this.titreQuestion = 'Creer ';
    }
  }

  updateQuestion() {
    // console.log(this.questions);
    if (
      this.questions.question === '' ||
      this.questions.all_reponse.length === 0 ||
      this.questions.true_reponse.length === 0 ||
      this.questions.identifiant === '' ||
      this.questions.matiere === ''
    ) {
      this.messageSuccess =
        "Verifier s'il ya un champs manqué ou des réponses non séléctionnés";
    } else {
      this.questionService
        .updateQuestion(this.paramRouter.snapshot.params.id, this.questions)
        .then((res: any) => {
          if (res.success) {
            this.messageSuccess = 'Modification avec success';
            setTimeout(() => {
              this.messageSuccess = '';
              this.router.navigate(['/dashboard/examen']);
            }, 2000);
          }
        });
    }
  }
}
