import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EtudiantService } from 'src/app/services/etudiant.service';
@Component({
  selector: 'app-add-etudiant',
  templateUrl: './add-etudiant.component.html',
  styleUrls: ['./add-etudiant.component.scss'],
})
export class AddEtudiantComponent implements OnInit {
  minDate: Date | undefined;
  maxDate: Date | undefined;
  title: String = '';
  message: Boolean = false;
  mess: String = '';
  public formGroup: any = FormGroup;
  etudiant: any = {
    nom: '',
    numero: '',
    parcours: '',
    niveau: '',
    dateNaiss: '',
    pseudo: '',
    password: '',
    role: '',
    matricule: '',
  };
  constructor(
    public dialogRef: MatDialogRef<AddEtudiantComponent>,
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
    private etudiantService: EtudiantService,
    public router: Router
  ) {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 39, 0, 1);
    this.maxDate = new Date(currentYear - 16, 11, 31);
  }

  ngOnInit(): void {
    this.title = this.dataDialog.type;
    this.isOpened();
  }

  isOpened() {
    if (this.dataDialog.type === 'add') {
      this.etudiant = {
        nom: '',
        numero: '',
        parcours: '',
        niveau: '',
        dateNaiss: Date,
        pseudo: '',
        password: '',
        role: '',
        matricule: '',
      };
    } else if (this.dataDialog.type == 'update') {
      this.etudiantService
        .getEtudiantByID(this.dataDialog.id)
        .then((res: any) => {
          this.etudiant = {
            nom: res.nom,
            numero: res.numero,
            parcours: res.parcours,
            niveau: res.niveau,
            dateNaiss: res.dateNaiss,
            pseudo: res.pseudo,
            password: res.password,
            role: res.role,
            matricule: res.matricule,
          };
        });
    }
  }

  addEtudiant() {
    // this.formatDate();
    this.etudiantService.addEtudiant(this.etudiant).then((res: any) => {
      console.log(res);
      if (res.message) {
        this.message = true;
        this.mess = res.message;
        setTimeout(() => {
          this.dialogRef.close();
        }, 2000);
      } else if (res.success == false) {
        this.message = false;
      }
    });
  }
  updateEtudiant() {
    // this.formatDate();
    if (
      this.etudiant.nom === '' ||
      this.etudiant.numero == null ||
      this.etudiant.parcours == '' ||
      this.etudiant.niveau == '' ||
      this.etudiant.dateNaiss == '' ||
      this.etudiant.role == '' ||
      this.etudiant.pseudo == '' ||
      this.etudiant.matricule == ''
    ) {
    } else {
      this.etudiantService
        .updateEtudiant(this.dataDialog.id, this.etudiant)
        .then((res: any) => {
          if (res.success) {
            this.message = true;
            this.mess = 'Modification avec success';
            setTimeout(() => {
              this.dialogRef.close();
            }, 2000);
          }
        });
    }
  }
}
