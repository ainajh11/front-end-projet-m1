import { Component, OnInit } from '@angular/core';
import { EtudiantService } from './../../services/etudiant.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddEtudiantComponent } from '../add-etudiant/add-etudiant.component';

@Component({
  selector: 'app-list-etudiant',
  templateUrl: './list-etudiant.component.html',
  styleUrls: ['./list-etudiant.component.scss'],
})
export class ListEtudiantComponent implements OnInit {
  public etudiants: any[] = [];
  niveauValue: string = '';
  parcoursValue: string = '';

  constructor(
    private etudiantService: EtudiantService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getAllEtudiants();
  }
  getAllEtudiants() {
    this.etudiantService.getAllEtudiant().then((res: any) => {
      this.etudiants = res;
    });
  }

  openAddDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    dialogConfig.height = '65%';
    dialogConfig.data = { type: 'add' };
    const openDial = this.dialog.open(AddEtudiantComponent, dialogConfig);
    openDial;
    openDial.afterClosed().subscribe(() => {
      this.getAllEtudiants();
    });
  }
  openEditDialog(id: String) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '65%';
    dialogConfig.data = { type: 'update', id: id };
    const openDial = this.dialog.open(AddEtudiantComponent, dialogConfig);
    openDial;
    openDial.afterClosed().subscribe(() => {
      this.getAllEtudiants();
    });
  }

  deleteEtudiant(id: String) {
    this.etudiantService.deleteEtudiant(id).then((res: any) => {
      this.getAllEtudiants();
    });
  }

  searchByNiveau() {
    // console.log(this.niveauValue + this.parcoursValue);
    if (this.niveauValue != '' && this.parcoursValue != '') {
      this.etudiantService
        .searchEtudiantByNivParc(this.niveauValue, this.parcoursValue)
        .then((res: any) => {
          // console.log(res.length);

          this.etudiants = res;
        });
    }
  }

  refresh() {
    this.getAllEtudiants();
    this.niveauValue = '';
    this.parcoursValue = '';
  }
}
