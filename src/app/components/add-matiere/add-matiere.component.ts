import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatiereService } from 'src/app/services/matiere.service';
@Component({
  selector: 'app-add-matiere',
  templateUrl: './add-matiere.component.html',
  styleUrls: ['./add-matiere.component.scss'],
})
export class AddMatiereComponent implements OnInit {
  title: String = '';
  message: Boolean = false;
  mess: String = '';
  public formGroup: any = FormGroup;
  matiere: any = {
    libelle: '',
    coefficient: '',
    parcours: '',
    niveau: '',
  };
  constructor(
    public dialogRef: MatDialogRef<AddMatiereComponent>,
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
    private matiereService: MatiereService
  ) {}

  ngOnInit(): void {
    this.title = this.dataDialog.type;
    this.isOpened();
  }

  isOpened() {
    if (this.dataDialog.type === 'add') {
      this.matiere = {
        nom: '',
        numero: '',
        parcours: '',
        niveau: '',
      };
    } else if (this.dataDialog.type == 'update') {
      this.matiereService
        .getMatiereByID(this.dataDialog.id)
        .then((res: any) => {
          this.matiere = {
            libelle: res.libelle,
            coefficient: res.coefficient,
            parcours: res.parcours,
            niveau: res.niveau,
          };
        });
    }
  }

  addEtudiant() {
    this.matiereService.addMatiere(this.matiere).then((res: any) => {
      console.log(res);
      if (res.success) {
        this.message = true;
        this.mess = res.data;
        setTimeout(() => {
          this.dialogRef.close();
        }, 2000);
      } else if (res.success == false) {
        this.message = false;
      }
    });
  }
  updateEtudiant() {
    // console.log(this.etudiant);

    if (
      this.matiere.libelle === '' ||
      this.matiere.coefficient == null ||
      this.matiere.parcours == '' ||
      this.matiere.niveau == ''
    ) {
    } else {
      this.matiereService
        .updateMatiere(this.dataDialog.id, this.matiere)
        .then((res: any) => {
          console.log(res);

          if (res.success) {
            this.message = true;
            this.mess = 'Modification avec success';
            setTimeout(() => {
              this.dialogRef.close();
            }, 2000);
          }
        });
    }
  }
}
