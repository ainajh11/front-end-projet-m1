import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ListEtudiantComponent } from '../components/list-etudiant/list-etudiant.component';
import { ListMatiereComponent } from '../components/list-matiere/list-matiere.component';
import { ListNoteComponent } from '../components/list-note/list-note.component';
//angular material
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { ListExamenComponent } from '../components/list-examen/list-examen.component';
import { AddExamenComponent } from '../components/add-examen/add-examen.component';

@NgModule({
  declarations: [
    ListEtudiantComponent,
    ListMatiereComponent,
    ListNoteComponent,
    ListExamenComponent,
    AddExamenComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,

    //material
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    MatRadioModule,
    MatInputModule,
    MatFormFieldModule,
    MatStepperModule,
    MatMenuModule,
    MatToolbarModule,
    MatAutocompleteModule,
    MatExpansionModule,
    MatListModule,
    MatSelectModule,
    MatDatepickerModule,
  ],
  exports: [
    ListEtudiantComponent,
    ListMatiereComponent,
    ListNoteComponent,
    ListExamenComponent,
    AddExamenComponent,
  ],
})
export class SharedModule {}
