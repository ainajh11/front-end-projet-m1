import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AddEtudiantComponent } from 'src/app/components/add-etudiant/add-etudiant.component';
import { AuthService } from 'src/app/services/auth.service';
import { IndexeddbService } from 'src/app/services/idb.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public formGroup: any = FormGroup;
  message = '';
  constructor(
    private authService: AuthService,
    private idbService: IndexeddbService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      pseudo: new FormControl('admin', [Validators.required]),
      password: new FormControl('admin', [Validators.required]),
    });
  }

  loginProcess() {
    this.authService.login(this.formGroup.value).subscribe(
      (res: any) => {
        console.log(res);
        if (res.success) {
          this.addToken(res.token);
          this.addUserInfo(res.user);
          if (res.user.role === 'admin') {
            this.router.navigate(['/dashboard']);
          } else if (res.user.role === 'etudiant') {
            this.router.navigate(['/examen']);
          }
        } else {
          this.message = res.message;
          setTimeout(() => {
            this.message = '';
          }, 6000);
        }
        // this.addToken(res.token);
        // this.router.navigateByUrl('/examen');
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }
  addToken(token: any) {
    this.idbService.set('token_exam', token);
  }
  addUserInfo(user: any) {
    this.idbService.set('user_info', user);
  }
  login() {
    this.router.navigateByUrl('/examen');
  }

  authentificationModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '70%';
    dialogConfig.height = '85%';
    dialogConfig.data = { type: 'authentification' };
    const openDial = this.dialog.open(AddEtudiantComponent, dialogConfig);
    openDial;
    openDial.afterClosed().subscribe(() => {
      console.log('closed');
    });
  }
}
