import { Component, OnInit } from '@angular/core';
import { IndexeddbService } from 'src/app/services/idb.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss'],
})
export class WrapperComponent implements OnInit {
  isExpanded: Boolean = true;
  constructor(private idb: IndexeddbService, public router: Router) {}

  ngOnInit(): void {}
  logOut() {
    // this.idb.delete('token_exam');
    // this.idb.delete('user_info');
    this.removeToken('token_exam');
    this.removeToken('user_info');
    this.router.navigate(['/']);
  }

  removeToken(token: any) {
    this.idb.delete(token);
  }
}
