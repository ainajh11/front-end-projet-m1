import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing-module';
import { EtudiantComponent } from './etudiant/etudiant.component';
import { WrapperComponent } from './wrapper/wrapper.component';
import { ExamenComponent } from './examen/examen.component';
import { MatiereComponent } from './matiere/matiere.component';
import { NoteComponent } from './note/note.component';
import { UserComponent } from './user/user.component';
import { AcceuilComponent } from './acceuil/acceuil.component';

// import Material
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';

import { SharedModule } from 'src/app/shared/shared.module';
import { NewExamComponent } from './new-exam/new-exam.component';

@NgModule({
  declarations: [
    EtudiantComponent,
    WrapperComponent,
    ExamenComponent,
    MatiereComponent,
    NoteComponent,
    UserComponent,
    AcceuilComponent,
    NewExamComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,

    //Ng Material
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    SharedModule,
    MatButtonModule,
  ],
})
export class DashboardModule {}
