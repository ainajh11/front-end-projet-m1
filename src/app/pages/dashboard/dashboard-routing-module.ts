import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddExamenComponent } from 'src/app/components/add-examen/add-examen.component';
import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { EtudiantComponent } from './etudiant/etudiant.component';
import { ExamenComponent } from './examen/examen.component';
import { MatiereComponent } from './matiere/matiere.component';
import { NoteComponent } from './note/note.component';
import { UserComponent } from './user/user.component';
import { WrapperComponent } from './wrapper/wrapper.component';
const routes: Routes = [
  {
    path: '',
    component: WrapperComponent,
    canActivate: [AuthGuardService],
    children: [
      // {
      //   path: '',
      //   component: AcceuilComponent,
      // },
      {
        path: '',
        component: EtudiantComponent,
      },
      {
        path: 'matiere',
        component: MatiereComponent,
      },
      {
        path: 'note',
        component: NoteComponent,
      },
      {
        path: 'examen',
        component: ExamenComponent,
      },
      {
        path: 'new-examen',
        component: AddExamenComponent,
      },
      {
        path: 'update-examen/:id',
        component: AddExamenComponent,
      },
      {
        path: 'user',
        component: UserComponent,
      },
    ],
  },
  {
    path: '**',
    component: PageNotFoundComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
