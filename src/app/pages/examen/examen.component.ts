import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EtudiantService } from 'src/app/services/etudiant.service';
import { ExamenService } from 'src/app/services/examen.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-examen',
  templateUrl: './examen.component.html',
  styleUrls: ['./examen.component.scss'],
})
export class ExamenComponent implements OnInit {
  public isExam: boolean = false;
  public isStart: boolean = false;
  public isNote: boolean = false;
  public matricule: string = '';
  public semestre: string = '1';
  public refExam: string = '';
  public time: number = 0;
  public note: number = 0;
  public etudiant: any;
  public interval: any;
  public matiere: any;
  public questions: any[] = [];
  public workTime = '';
  public exam: any[] = [];
  constructor(
    public router: Router,
    private etudiantService: EtudiantService,
    private questionService: QuestionService,
    private examenService: ExamenService
  ) {}

  ngOnInit(): void {}

  validateEtudiant() {
    this.etudiantService.getByMatricule(this.matricule).then((res) => {
      if (res.length > 0) {
        this.questionService
          .getQuestionIdentifiant(this.refExam)
          .then((exam) => {
            console.log(exam[0].length);

            if (exam.length > 0) {
              this.time = exam[0][0].time;
              this.matiere = exam[0][0].matiere[0];
              this.semestre = exam[0][0].semestre;
              console.log(this.matiere);

              this.etudiant = res[0];
              this.questions = exam[0];
              this.isExam = true;
            } else {
            }
          });
      } else {
      }
      console.log('matricule: ', res);
    });
  }

  start() {
    this.isStart = true;
    this.time--;
    let minute = 59,
      seconde = 60;
    this.interval = setInterval(() => {
      seconde--;
      if (seconde === 0) {
        if (minute > 0) {
          minute--;
          seconde = 60;
        }
      }
      if (minute === 0) {
        if (this.time > 0) {
          this.time--;
          minute = 60;
        }
      }

      this.workTime =
        '0' +
        this.time +
        ':' +
        (minute < 10 ? '0' + minute : minute) +
        ':' +
        (seconde < 10 ? '0' + seconde : seconde);
      if (this.time === 0 && minute === 0 && seconde === 0) {
        console.log('capreo');
      }
    }, 1000);
  }

  chooseResponse(event: any, _id: any, response: any, trueResponse: any) {
    const idx = this.exam.findIndex((ex) => ex._id === _id);

    if (event.target.checked) {
      const data = {
        _id: _id,
        response: [response],
        trueResponse: trueResponse,
      };
      if (idx === -1) {
        this.exam.push(data);
      } else {
        this.exam[idx].response.push(response);
      }
    } else {
      if (idx !== -1) {
        const idxR = this.exam[idx].response.findIndex(
          (r: any) => r === response
        );
        if (idxR !== -1) {
          this.exam[idx].response.splice(idxR, 1);
        }
      }
    }
    console.log(this.exam);
    // console.log(question);
    // console.log(response);
  }

  validate() {
    this.note = 0;
    clearInterval(this.interval);
    const plus = 20 / this.questions.length;
    for (const ex of this.exam) {
      if (ex.response.length === ex.trueResponse.length) {
        for (const respt of ex.trueResponse) {
          console.log(respt);

          for (const resp of ex.response) {
            console.log(resp);

            if (respt === resp) {
              this.note += plus;
            }
          }
        }
      }
    }
    this.isNote = true;
    this.examenService
      .addNote(this.note, this.matiere._id, this.etudiant._id, this.semestre)
      .then(() => {
        console.log('add');
      });
    console.log('note: ', this.note);
  }

  goToNote() {
    this.router.navigate(['note', this.etudiant._id]);
  }
}
