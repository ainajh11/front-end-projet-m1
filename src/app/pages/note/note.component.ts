import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ExamenService } from 'src/app/services/examen.service';
@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],
})
export class NoteComponent implements OnInit {
  id: string = '';
  note: any[] = [];
  etudiant: any;
  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private noteService: ExamenService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.noteService.getByEtudiant(this.id).then((res: any) => {
      console.log(res);
      this.etudiant = res[0].etudiant;
      this.note = res;
    });
    console.log(this.id);
  }
}
