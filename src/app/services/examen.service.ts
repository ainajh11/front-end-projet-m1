import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IndexeddbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class ExamenService {
  constructor(private http: HttpClient, private idbService: IndexeddbService) {}
  public getAllNote() {
    return this.http.get(`${environment.URL_LOCAL}/examen`).toPromise();
  }
  public getByEtudiant(id: string) {
    return this.http
      .get(`${environment.URL_LOCAL}/examen/etudiant/${id}`)
      .toPromise();
  }
  public addNote(
    note: number,
    matiere: string,
    etudiant: string,
    semestre: string
  ) {
    return this.http
      .post(`${environment.URL_LOCAL}/examen`, {
        note,
        matiere,
        etudiant,
        semestre,
      })
      .toPromise();
  }
}
