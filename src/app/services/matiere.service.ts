import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IndexeddbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class MatiereService {
  constructor(private http: HttpClient, private idbService: IndexeddbService) {}

  public getAllMatiere() {
    return this.http.get(`${environment.URL_LOCAL}/matiere`).toPromise();
  }

  public async addMatiere(data: any) {
    let token: any;
    await this.idbService.get('token_exam').then((tok: any) => {
      token = tok;
    });
    return this.http
      .post(`${environment.URL_LOCAL}/matiere`, data, {
        headers: {
          authorization: token,
        },
      })
      .toPromise();
  }

  public updateMatiere(id: any, data: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      let token: any;
      await this.idbService.get('token_exam').then((tok) => {
        token = tok;
      });
      this.http
        .put(`${environment.URL_LOCAL}/matiere/${id}`, data, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response: any) => {
          resolve(response);
        });
    });
  }

  public deleteMatiere(id: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      let token: any;
      await this.idbService.get('token_exam').then((tok) => {
        token = tok;
      });
      this.http
        .delete(`${environment.URL_LOCAL}/matiere/${id}`, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response: any) => {
          resolve(response);
        });
    });
  }

  public getMatiereByID(id: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      this.http
        .get(`${environment.URL_LOCAL}/matiere/${id}`)
        .subscribe((response) => {
          resolve(response as any[]);
        });
    });
  }
  public searchMatiereByNivParc(
    niveau: String,
    parcours: String
  ): Promise<any[]> {
    return new Promise(async (resolve) => {
      this.http
        .get(`${environment.URL_LOCAL}/matiere/search/${niveau}/${parcours}`)
        .subscribe((response) => {
          resolve(response as any[]);
        });
    });
  }
}
