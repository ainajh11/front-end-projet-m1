import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IndexeddbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class EtudiantService {
  constructor(private http: HttpClient, private idbService: IndexeddbService) {}

  public getAllEtudiant() {
    return this.http.get(`${environment.URL_LOCAL}/user/etudiant`).toPromise();
  }

  public async addEtudiant(data: any) {
    return this.http
      .post(`${environment.URL_LOCAL}/user/register`, data)
      .toPromise();
  }

  public updateEtudiant(id: any, data: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      let token: any;
      await this.idbService.get('token_exam').then((tok) => {
        token = tok;
      });
      this.http
        .put(`${environment.URL_LOCAL}/user/etudiant/${id}`, data, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response: any) => {
          resolve(response);
        });
    });
  }

  public deleteEtudiant(id: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      let token: any;
      await this.idbService.get('token_exam').then((tok) => {
        token = tok;
      });
      this.http
        .delete(`${environment.URL_LOCAL}/user/${id}`, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response: any) => {
          resolve(response);
        });
    });
  }

  public getEtudiantByID(id: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      this.http
        .get(`${environment.URL_LOCAL}/user/${id}`)
        .subscribe((response) => {
          resolve(response as any[]);
        });
    });
  }
  public getByMatricule(matricule: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      this.http
        .get(`${environment.URL_LOCAL}/user/matricule/${matricule}`)
        .subscribe((response) => {
          resolve(response as any[]);
        });
    });
  }
  public searchEtudiantByNivParc(
    niveau: String,
    parcours: String
  ): Promise<any[]> {
    return new Promise(async (resolve) => {
      this.http
        .get(
          `${environment.URL_LOCAL}/user/etudiant/search/${niveau}/${parcours}`
        )
        .subscribe((response) => {
          resolve(response as any[]);
        });
    });
  }
}
