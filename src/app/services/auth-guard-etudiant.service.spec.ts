import { TestBed } from '@angular/core/testing';

import { AuthGuardEtudiantService } from './auth-guard-etudiant.service';

describe('AuthGuardEtudiantService', () => {
  let service: AuthGuardEtudiantService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthGuardEtudiantService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
