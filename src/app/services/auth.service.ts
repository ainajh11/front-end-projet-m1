import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { IndexeddbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private idbService: IndexeddbService) {}

  login(data: any) {
    return this.http.post(`${environment.URL_LOCAL}/user/login`, data);
  }

  decodedToken(data: any) {
    return this.http.post(`${environment.URL_LOCAL}/decodedToken`, data);
  }

  signup(data: any[]) {
    return this.http.post(`${environment.URL_LOCAL}/user/register`, data);
  }

  async current() {
    let token: any;
    await this.idbService.get('token').then((tok) => {
      token = tok;
    });
    this.http
      .get(`${environment.URL_LOCAL}/user/current`, {
        headers: {
          authorization: token,
        },
      })
      .toPromise();
  }
}
