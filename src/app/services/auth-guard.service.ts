import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { IndexeddbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor(public router: Router, private idb: IndexeddbService) {}

  async canActivate(): Promise<boolean> {
    this.isActivate();
    if (await this.isActivate()) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }

  async isActivate(): Promise<boolean> {
    return await this.idb.get('user_info').then((res: any) => {
      if (res == undefined) {
        res = false;
      } else {
        if (res.role == 'admin') {
          res = true;
        }
      }
      return res;
    });
  }
}
