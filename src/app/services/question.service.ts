import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IndexeddbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class QuestionService {
  constructor(private http: HttpClient, private idbService: IndexeddbService) {}
  public getAllQuestions() {
    return this.http.get(`${environment.URL_LOCAL}/question`).toPromise();
  }

  public async addQuestion(data: any) {
    let token: any;
    await this.idbService.get('token_exam').then((tok: any) => {
      token = tok;
    });
    return this.http
      .post(`${environment.URL_LOCAL}/question`, data, {
        headers: {
          authorization: token,
        },
      })
      .toPromise();
  }

  public updateQuestion(id: any, data: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      let token: any;
      await this.idbService.get('token_exam').then((tok) => {
        token = tok;
      });
      this.http
        .put(`${environment.URL_LOCAL}/question/${id}`, data, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response: any) => {
          resolve(response);
        });
    });
  }

  public deleteQuestion(id: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      let token: any;
      await this.idbService.get('token_exam').then((tok) => {
        token = tok;
      });
      this.http
        .delete(`${environment.URL_LOCAL}/question/${id}`, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response: any) => {
          resolve(response);
        });
    });
  }

  public getQuestionID(id: string): Promise<any[]> {
    return new Promise(async (resolve) => {
      this.http
        .get(`${environment.URL_LOCAL}/question/${id}`)
        .subscribe((response) => {
          resolve(response as any[]);
        });
    });
  }
  public getQuestionIdentifiant(id: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      this.http
        .get(`${environment.URL_LOCAL}/question/by-identifiant/${id}`)
        .subscribe((response) => {
          resolve(response as any[]);
        });
    });
  }
}
